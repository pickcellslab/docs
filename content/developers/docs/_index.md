+++

title ="Update Documentation"
weight = 7

date = "2018-04-24"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Mike Jackson"
lastmod = "2018-07-12"

repo = "docs"
fileUrl = "https://framagit.org/pickcellslab/docs/tree/master/content/developers/docs/_index.md"

tags = ["documentation", "develop"]

+++

This page explains how this website is generated and how new pages can be added.

---

## Contents

* [Key components](#key-components)
* [Adding documentation directories](#adding-documentation-directories)
* [Creating documentation files](#creating-documentation-files)
  - [Defining file headers](#defining-file-headers)
  - [Adding links to other pages](#adding-links-to-other-pages)
  - [Adding links to headings in the same page](#adding-links-to-headings-in-the-same-page)
  - [Adding links to headings in other pages](#adding-links-to-headings-in-other-pages)
  - [Adding icons](#adding-icons)
  - [Adding block diagrams](#adding-block-diagrams)
  - [Provide meaningful link text](#provide-meaningful-link-text)
  - [Adding images](#adding-images)
  - [Syntax highlighting](#syntax-highlighting)
  - [Adding a table of contents](#adding-a-table-of-contents)
  - [Adding links to child pages](#adding-links-to-child-pages)
* [Building documentation locally](#building-documentation-locally)
* [Checking website build status](#checking-website-build-status)

---

## Key components

The website source files are held in our [docs](https://framagit.org/pickcellslab/docs) repository.

The website is generated using [Hugo](https://gohugo.io/).

The theme is  [DocDock](https://docdock.netlify.com/), which is installed as a submodule of the `docs` repository.

The website is built using a [GitLab Continuous Integration](https://about.gitlab.com/features/gitlab-ci-cd/) (GitLab CI) job, defined in the `docs` file [.gitlab-ci.yml](https://framagit.org/pickcellslab/docs/blob/master/.gitlab-ci.yml). This job fetches documentation from other PickCells repositories in the [pickcellslab](https://framagit.org/pickcellslab) project.

The `docs` repository contains:

* Scripts and configuration required to build the web site.
* This page itself, in a ` docs` directory.

The other PickCells repositories are expected to hold their documentation in a `docs` directory. At present, these are held within `docs` branches.

---

## Adding documentation directories

Documentation files must be located inside a `docs` folder of a repository. For example, see the [foundationj](https://framagit.org/pickcellslab/foundationj/tree/docs) repository.

The structure of this directory is guided by DocDock's [content organisation](https://themes.gohugo.io/theme/docdock/content-organisation/). You may find it useful to also refer to [Hugo: Leaf and Branch Bundles](https://scripter.co/hugo-leaf-and-branch-bundles/). 

A typical directory structure is expected to look something like this:

```
docs
 |
 -> content (This directory should contain Markdown pages only)
     |
     -> sub_directory (usually 'developers' or 'use', see the menu hierarchy on the left )
         |
         -> custom_directory (your custom directory with your documentation)
             |
             -> _index.md (this file adds a menu entry and lists any subpages)
             |
             -> page1.md 
             |
             -> page2.md 
 |
 -> static (directory for resources such as images)
     |
     -> sub_directory (this folder hierarchy mirrors the one found in the 'content' directory)
         |
         -> custom_directory
             |
             -> image1.png (image used in page1 or page2)
             |
             -> image2.png (image used in page1 or page2)
```

---

## Creating documentation files

Documentation files are written using [Markdown](https://daringfireball.net/projects/markdown/syntax) complemented by Hugo [shortcodes](https://gohugo.io/content-management/shortcodes/) and DocDock [shortcodes](https://themes.gohugo.io/theme/docdock/shortcodes/).

Markdown files have the suffix `.md`.

### Defining file headers

Metadata can be attached to each content file and is defined as a [TOML](https://en.wikipedia.org/wiki/TOML) header. A typical page header can look something like this:

```toml
+++
title ="A Title" # This is the title listed in the Menu
weight = 1       # Defines the position of the menu item

date = "2018-04-15"                 # Creation date
creatorDisplayName = "Name Surname" # Creator Name

lastmodifierdisplayname = "Name Surname" # Last Modifier Name
lastmod = "2018-04-20"                   # Last Modified date

repo = "repo-name" # Name of the repository containing the file (for example pickcells-api)
fileUrl = "https://domain.org/path/to/file/_index.md" # URL of the current file

tags = ["tag 1", "tag 2"] # Tags e.g. "develop", "community" etc.
+++
```

For a list of variables, see Hugo's [front matter](https://gohugo.io/content-management/front-matter/) and DocDock's [Create Page](https://themes.gohugo.io/theme/docdock/create-page/).

### Adding links to other pages

To add a link to another page in the documentation, add:

```
[link text](<RELATIVE-PATH-TO-PAGE>)
```

where `/<RELATIVE-PATH-TO-PAGE>` is the relative path to the page from the `content` directory e.g. `sub_directory/custom_directory/` for the file organisation shown above. It does not matter if the page is held within a different repository, they are all brought together into a common directory when the website is built.

Examples of such links used in in the documentation include:

```
[Set up a PickCells development environment](/developers/dev_core/setup)
[Collaborative Development](/developers/collab_dev).
```

### Adding links to headings in the same page

Anchors, or "slugs", are automatically created for each heading e.g. for a heading "This is a heading" the anchor is `#this-is-a-heading`. The anchor is the heading with all the text in lower-case, and all non-alphabetical and non-numerical characters (e.g. "`,`", "` `", "`.`", "`?`" etc.) replaced by a dash `-`.

To link to a heading in the current page, specify the heading anchor e.g.

```
[link text](#<ANCHOR>)
```

Examples of such links used in in the documentation include:

```
[Installation Requirements](#installation-requirements) 
```

### Adding links to headings in other pages

To link to a heading in the other page, specify the heading anchor e.g.

```
[link text](<RELATIVE-PATH-TO-PAGE>#<ANCHOR>)
```

Examples of such links used in in the documentation include:

```
[Get PickCells repositories](/developers/dev_core/setup/#get-pickcells-repositories)
[Managing multiple developers and version control](/developers/collab_dev/#managing-multiple-developers-and-version-control)
```

### Adding icons

Many icons are readily available via the [Font Awesome 4 library](https://simplesvg.com/icon-sets/fa/).

To add an icon, you need to use a shortcode. For example, to add this icon {{< icon name="film" size="large">}}, use the following shortcode:

```
{{</* icon name="film" size="large" */>}}
```

See Hugo's shortcodes for [icon](https://themes.gohugo.io/theme/docdock/shortcodes/icon/)s.

For less-than-or-equal-to and greater-than-or-equal to you can use Unicode HTML entities:

* &#8804;: `&#8804;`
* &#8805;: `&#8805;`

### Adding block diagrams

[Mermaid](https://mermaidjs.github.io/) provides a syntax to generate flow charts and sequence diagrams, including nodes as hyperlinks. For example see the [Create Plugins](/developers/plugins/) page and its source in the [_index.md](https://framagit.org/pickcellslab/pickcells-api/blob/docs/docs/content/developers/plugins/_index.md) file in the [pickcells-api](https://framagit.org/pickcellslab/pickcells-api/blob/docs/docs/content/developers/plugins/_index.md) repository.

For other examples, see the DocDoc [Mermaid](https://themes.gohugo.io/theme/docdock/shortcodes/mermaid/) examples.

A very useful tool is the [Mermaid live editor](https://mermaidjs.github.io/mermaid-live-editor/).

### Provide meaningful link text

When adding text for a link do not use "this page" or "here" as these do not provide any context for the reader. Provide meaningful link text. For example, compare:

```
Hugo's shortcodes are [here](https://themes.gohugo.io/theme/docdock/shortcodes/icon/)s.

See Hugo's shortcodes for [icon](https://themes.gohugo.io/theme/docdock/shortcodes/icon/)s.
```

### Adding images

You can add an image using the following code:
  
```     
![Some description](/<RELATIVE-PATH>/<IMAGE>.png)
```

where `/<RELATIVE-PATH>` is the relative path to the image from the `static` directory e.g. `sub_directory/custom_directory/image1.png` for the file organisation shown above. It does not matter if the image file is held within a different repository, they are all brought together into a common directory when the website is built.

This will place the image in the middle of the screen.  

#### Formatting images

You can also apply [Cascading Style Sheets](https://en.wikipedia.org/wiki/Cascading_Style_Sheets) (CSS) classes to an image by adding a classes html tag: 
  
```
![Some description](/<RELATIVE-PATH>/<IMAGE>.png?classes=left,border,shadow)
```

This code will place your image to the left of the screen, add a border and drop a shadow.  

Available CSS classes can be found in the `docs` repository under [/static/css](https://framagit.org/pickcellslab/docs/tree/master/static/css).

#### Inlining images

Images can be inlined in text as follows:

```
![Some description](/<RELATIVE-PATH>/<IMAGE>.png?classes=inline)
```
  
#### Sizing images

The size of an image can be defined as follows: 
  
```
![Some description](/<RELATIVE-PATH>/<IMAGE>.png?height=80px)
```

#### Creating image galleries

A gallery of images which will display all images contained in a folder can be created with the following shortcode:

```
{{</* gallery dir="/path/to/image/directory" / */>}} {{</* load-photoswipe */>}}
```

Alternatively, a gallery can be created using individual files:

```
{{</* gallery caption-effect="none" */>}}
{{</* figure link="/path/to/image/directory/image1.png" caption=" Description of Image 1" */>}}
{{</* figure link="/path/to/image/directory/image2.png" caption=" Description of Image 2" */>}}
{{</* /gallery */>}}

```

This feature relies on 'Hugo Easy Gallery' shortcodes created by Li-Wen Yip. A demo and additional documentation on how to use this feature is available on the [Hugo Easy Gallery](https://www.liwen.id.au/heg/). 

### Syntax highlighting

Hugo uses [pygment](http://pygments.org/) for syntax highlighting. If you want code sections to be highlighted, you will need to define the language you are using as autodetection is not enabled. For example: 

    ```java 
    public static void main(String[] args) ...
    ```  


### Adding a table of contents

A table of contents can be created as follows.

Search for all the headings. For example:

```
grep \#\# seg_editor.md 
```

This will extract all the headings e.g.

```
## Contents
## Purpose
## Activation Conditions
## Installation Requirements
## Tutorial
### Overview
### Selecting and Deselecting Objects
### Default Controls
### Undo Controls
### Erase, Expand and Addition Controls
### Edits Count
```

Edit the text into a list of links e.g.

```
## Contents

* [Purpose](#purpose) 
* [Activation Conditions](#activation-conditions) 
* [Installation Requirements](#installation-requirements) 
* [Tutorial](#tutorial) 
  - [Overview](#overview) 
  - [Selecting and Deselecting Objects](#selecting-and-deselecting-objects)
  - [Default Controls](#default-controls) 
  - [Undo Controls](#undo-controls) 
  - [Erase, Expand and Addition Controls](#erase-expand-and-addition-controls)
  - [Edits Count](#edits-count) 
```


### Adding links to child pages

For `_index.md` pages with no content, Hugo and DocDock will automatically add links and summaries of the child pages. The summaries are the first 70 words of each child page, but with no formatting, so, unfortunately, the content from the child pages can look messy.

If you want to add a list of the child pages only, add, to an `_index.md` file:

```
{{%/* children style="li" sort="weight" */%}} 
```

`style` controls how the child page names are shown by specifying the HTML element to use. Here a list, `li` is used. An alternative is, for example, `h2`.

If you want to show the summaries, nicely formatted, too then, to each child page, add a summary divider after the content you want to be in the summary:

```
<!-- more -->
```

In the parent `_index.md` file, add

```
{{%/* children style="li" description="true" sort="weight" */%}} 
```

For more information, see DocDock's [children](https://themes.gohugo.io/theme/docdock/shortcodes/children/) shortcode and Hugo's [content summaries](https://gohugo.io/content-management/summaries/).

---

## Building documentation locally

If you would like to build the documentation locally you can.

Fork and clone the `docs` repository, as described in [Fork a PickCells repository](/developers/dev_core/setup/):

* https://framagit.org/pickcellslab/docs

Copy the `scripts` folder of the `docs` repository to a folder outwith the docs repository e.g.

```
cp -r docs/scripts ~/local-docs
```

Download a stand-alone Hugo binary file, `hugo`, for your operating system from Hugo's [releases](https://github.com/gohugoio/hugo/releases).

Place this in your directory, e.g. `~/local-docs/`.

Change the permissions of `hugo` and the scripts, `*.sh`, so they can be executed e.g.

```
chmod +x ~/local-docs/*.sh
chmod +x ~/local-docs/hugo
```

Run the `get-repositories.sh` script to clone all the PickCells repositories, and checkout their `docs` branches:

```
./local-docs/get-repositories.sh
```

Run the `refresh.sh` script, within `local-docs`, to fetch content from the `docs` folders of your local clones of the PickCells repositories, passing as an argument the name of the folder in which you cloned all the PickCells repositories e.g.

```
cd local-docs/
./refresh.sh ..
```

Finally, generate the site using the following command: 

```
./hugo server --disableFastRender
```

Instead of generating the static website once only, the `hugo` command constantly regenerates the site based on changes to files in the working space, stores these in memory, and serves them up via a local URL, e.g. http://localhost:1313/docs/, that you can view in your browser and leave open as you make changes, refreshing as needed. 

The `--disableFastRender` flag makes sure it regenerates more things than is default. However, the server might need to be restarted to make sure it really renders the new version. You can restart the server by pressing Ctrl+C then rerunning the command above.

If using a virtual machine and you want to access your website from the virtual machine host, then try adding the flag:

```
-b http://<VM-IP-ADDRESS> --bind="0.0.0.0"
```

---

## Checking website build status

You can check to see whether a rebuild of the PickCells website has succeeded by checking the build [pipelines](https://framagit.org/pickcellslab/docs/pipelines) of the [docs](https://framagit.org/pickcellslab/docs/) repository.

### Troubleshooting

If a rebuild fails with an error like:

```
Building sites ...
ERROR 2018/11/28 17:40:45 render of "page" failed: execute of template
failed: template: _default/single.html:13:9: executing
"_default/single.html" at <partial (printf "%s/...>: error calling
partial:
"/builds/pickcellslab/docs/themes/docdock/layouts/partials/flex/body-beforecontent.html:2:10":
execute of template failed: template: ...
```

Then this may have arisen due to a change in Hugo version that deprecates features or syntax constructs used in the `layouts` files of the `docs` repository.

If this issue arises then:

1. Check the [Hugo news](https://gohugo.io/news/0.50-relnotes) for deprecation warnings.
2. Check the commits to the [docdock theme](https://github.com/vjeantet/hugo-theme-docdock) repository.

If there has been a change in the features or syntax constructs AND if docdock has been updated to reflect these changes, then:

1. Download the current version of Hugo.
2. Update the Git submodule for docdock in `docs`:

    ```
    cd themes/docdock/
    git checkout master
    ```

3. Update the files in `layouts` with any necessary changes.
4. Build the documentation locally with the latest version of Hugo.
5. If all is well then commit the changes:

    ```
    git add themes/docdock
    git commit -m "Updating submodule to latest" themes/docdock
    git add layouts
    git commit -m "Updated to be Hugo X.Y compatible" layouts
    ```

6. Push the changes to trigger a rebuild of the website:

    ```
    git push origin master
    ```
