+++

title ="Credits"
weight = 7

date = "2020-00-24"
creatorDisplayName = "Guillaume Blin"

repo = "docs"
fileUrl = "https://framagit.org/pickcellslab/docs/tree/master/content/docs/_index.md"

+++


## Credits

This website has been created with the help of [EPCC](https://www.epcc.ed.ac.uk/) and was funded by [ISSF3](https://www.ed.ac.uk/medicine-vet-medicine/research-support-development-commercialisation/issf)
