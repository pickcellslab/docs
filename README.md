# docs repository

This repository contains the configuration files to create the documentation website for PickCells.   

For information on how the PickCells documentation website is built and updated, see:

* [Update Documentation](content/developers/docs/_index.md)
