#!/bin/bash
#
# Usage: ./refresh.sh <SOURCE>
# 
# Check if <SOURCE>/docs exists and, if so, copy its contents 
# to the current directory.
# Iterate through a list of PickCells Git repository directories, 
# relative to <SOURCE>. 
# For each directory, check if docs/static/ and docs/content/ sub-directories 
# exist and have content. 
# If so, copy their contents to static/ and content/ in the 
# current directory.

# Check if a path has been passed as an argument.
if [ -z "$1" ]
then
    echo 'ERROR: Please provide the path to the pickcells repositories'
    exit 1
fi

# Check that the provided path is a directory.
if [ ! -d $1 ]   
then 
   echo 'ERROR: The provided path is not a directory'
   exit 1
fi

SOURCE=$1

# Get the current directory.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Clean up.
rm -rf $DIR/archetypes/
rm -rf $DIR/content/
rm -rf $DIR/layouts/
rm -rf $DIR/static/
rm -rf $DIR/themes/

# Copy content from repositories
echo "Copying docs"
if [ -d "$SOURCE/docs" ]; then
    if [ "$(ls -A $SOURCE/docs)" ]; then
        cp -r $SOURCE/docs/* $DIR
    else
        echo "ERROR: $SOURCE/docs is empty"
        exit 1
    fi
else
    echo "ERROR: $SOURCE/docs doesn't exist"
    exit 1
fi

./get-content.sh $SOURCE $DIR
