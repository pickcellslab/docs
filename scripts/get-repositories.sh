#!/bin/bash
#
# Usage: ./get-repositoriess.sh
#
# Iterate through a list of PickCells Git repository names and clone
# each repository to the current directory, checking out their "docs"
# branches.

for REPO in foundationj nessys pickcells-api pickcells-assembly pickcells-essentials pickcells-j3d pickcells-jfree pickcells-jzy3d pickcells-neodb pickcells-prefuse pickcells-rjava pickcells-tutorials; do
    git clone --branch docs https://gitlab-ci-token:${CI_JOB_TOKEN}@framagit.org/pickcellslab/$REPO.git
done
