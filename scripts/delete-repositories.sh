#!/bin/bash
#
# Usage: ./delete-repositoriess.sh
#
# Iterate through a list of PickCells Git repository names and remove
# the directory holding that repository from the current directory.

for REPO in foundationj nessys pickcells-api pickcells-assembly pickcells-essentials pickcells-j3d pickcells-jfree pickcells-jzy3d pickcells-neodb pickcells-prefuse pickcells-rjava pickcells-tutorials; do
    rm -rf $REPO
done
