#!/bin/bash
#
# Usage: ./get-content.sh <SOURCE> <DEST>
#
# Iterate through a list of PickCells Git repository directories,
# relative to <SOURCE>.
# For each directory, check if docs/static/ and docs/content/ sub-directories
# exist and have content.
# If so, copy their contents to <DEST>/static and <DEST>/content.

SOURCE=$1
DEST=$2

mkdir -p $DEST/content
mkdir -p $DEST/static

for REPO in foundationj nessys pickcells-api pickcells-assembly pickcells-essentials pickcells-j3d pickcells-jfree pickcells-jzy3d pickcells-neodb pickcells-prefuse pickcells-rjava pickcells-tutorials; do
    echo "Copying $REPO"
    for TYPE in content static; do
        echo "$REPO/docs/$TYPE"
        if [ -d "$SOURCE/$REPO/docs/$TYPE" ]; then
            if [ "$(ls -A $SOURCE/$REPO/docs/$TYPE)" ]; then
                cp -r $SOURCE/$REPO/docs/$TYPE/* $DEST/$TYPE
            else
                echo "WARN: $SOURCE/$REPO/docs/$TYPE is empty"
            fi
        else
            echo "WARN: $SOURCE/$REPO/docs/$TYPE doesn't exist"
        fi
    done
    echo ""
done
